FROM python:slim-buster

RUN apt update && \
    apt install -y build-essential
    #apt install -y linux-headers-amd64

WORKDIR /usr/app/src

COPY requirements.txt ./

RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt

COPY . .


CMD ["python","/usr/app/src/app.py"]

EXPOSE 5000
